<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
    <style>
        #form{
            width: 400px;
            height: auto;
            margin: 0 auto;
            border: rgba(22, 25, 16, 0.98);
            border: solid;
        }
        #form_inner{
            margin: 20px 20px 20px 20px;
        }
        input {
             width: 100%;
        }
        h1 {
            text-align: center;
        }


    </style>
</head>
<body>
<div id="form">
    <h1> Student Information</h1>
<div id="form_inner">
   <form action="process.php" method="post">
       Student ID: <input type="number" name="seid">
       Student Name: <input type="text" name="stname">
       Course Information: <br>
       Bengali :  <input type="number" name="bengali">
       English :   <input type="number" name="english">
       Math : <input type="number" name="math">
       <input id="submit" type="submit" value="Submit">
   </form>
</div>

</div>
</body>
</html>