<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grading System Using OOP</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
    <body>
        <form action="process.php" method="post" >
            <fieldset>
                <legend>Student's Information</legend>
                <label>Name of Student:</label>
                <input type="text" name="name"><br>
                <label>Student ID:     </label>
                <input type="number" name="studentId">
            </fieldset><br><br>
            <fieldset>
                <legend>Enter Result</legend>

                <label>Web Development:</label>
                <input type="number" name="webDevelop"><br>
                <label>Data Structure:</label>
                <input type="number" name="dataStructure"><br>
                <label>Object Oriented Programming:</label>
                <input type="number" name="oop"><br>
                <label>Structured Programming Language:</label>
                <input type="number" name="spl"><br>
                <label>Discrete Mathematics:</label>
                <input type="number" name="discreteMath"><br>
                <input type="submit" value="Show Result">
            </fieldset>
        </form>
    </body>
</html>