<?php


namespace App;


class StudentsInfo
{
    private $name;
    private $studentId;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setStudentId($studentId)
    {
        $this->studentId = $studentId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStudentId()
    {
        return $this->studentId;
    }
}