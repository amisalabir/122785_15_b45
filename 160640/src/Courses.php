<?php


namespace App;


class Courses
{

    private $markbangla;
    private $gradebangla;
    private $markmath;
    private $grademath;
    private $markenglish;
    private $gradeenglish;


    public function setMarkmath($markmath)
    {
        $this->markmath = $markmath;
    }

    public function setMarkenglish($markenglish)
    {
        $this->markenglish = $markenglish;
    }

    public function setGradeenglish()
    {

        $this->gradeenglish=$this->forGradeResult($this->markenglish);
    }

    public function setMarkbangla($markbangla)
    {
        $this->markbangla = $markbangla;
    }

    public function getMarkbangla()
    {
        return $this->markbangla;
    }

    public function getMarkenglish()
    {
        return $this->markenglish;
    }

    public function getMarkmath()
    {
        return $this->markmath;
    }

    public function setGradebangla()

    {

        $this->gradebangla =$this->forGradeResult($this->markbangla);
    }

    public function setGrademath()
    {

        $this->grademath =$this->forGradeResult($this->markmath);
    }

    public function getGradebangla()
    {

        return $this->gradebangla;
    }

    public function getGradeenglish()
    {

        return $this->gradeenglish;
    }

    public function getGrademath()
    {
      
        return $this->grademath;
    }

    public function forGradeResult($number)
    {

        switch($number){

            case($number>=80):
                $grade= "A+";
                break;
            case($number>=70):
                $grade=  "A";
                break;
            case($number>=60):
                $grade=  "A-";
                break;
            case($number>=50):
                $grade=  "B";
                break;
            case($number>=33):
                $grade=  "C";
                break;
            default:
                $grade=  "F";
                break;
        }

        return $grade;
    }

}

